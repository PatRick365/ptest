#include "ptest/ptest.h"

#include <cstdio>

static unsigned long tests_total = 0;
static unsigned long tests_failed = 0;

#define test_truth(x) do { ++tests_total; if (!static_cast<bool>(x)) { ++tests_failed; printf("test_truth(%s) failed %s:%d\n", #x, __FILE__, __LINE__); } } while(0)
#define test_false(x) do { ++tests_total; if (static_cast<bool>(x))  { ++tests_failed; printf("test_false(%s) failed %s:%d\n", #x, __FILE__, __LINE__); } } while(0)

void run_tests()
{
    {   // string
        char arr1[] = { 'A', 'B', 'C', '\0' };
        char arr2[] = { 'A', 'B', 'C', '\0' };
        char *ptr1 = arr1;
        char *ptr2 = arr2;
        test_truth(ptest::internal::comp_str(arr1, arr2));
        test_truth(ptest::internal::comp_str(ptr1, ptr2));
        test_truth(ptest::internal::comp_str(arr1, "ABC"));
        test_truth(ptest::internal::comp_str(ptr1, "ABC"));
        test_truth(ptest::internal::comp_str(arr1, ptr2));
        test_truth(ptest::internal::comp_str("ABC", "ABC"));
        test_false(ptest::internal::comp_str("ABC", "XYZ"));
        test_false(ptest::internal::comp_str(arr1, "XYZ"));
        test_false(ptest::internal::comp_str(ptr1, "XYZ"));
    }
    {   // double
        test_truth(ptest::internal::comp_float(1.0, 1.0));
        test_false(ptest::internal::comp_float(1.0, 2.0));
        test_false(ptest::internal::comp_float(1.1, 1.0));
        test_false(ptest::internal::comp_float(1.000000000000001, 1.0));
        test_truth(ptest::internal::comp_float(1.0000000000000001, 1.0));
    }
    {   // float
        test_truth(ptest::internal::comp_float(1.0F, 1.0F));
        test_false(ptest::internal::comp_float(1.0F, 2.0F));
        test_false(ptest::internal::comp_float(1.1F, 1.0F));
        test_false(ptest::internal::comp_float(1.0000001F, 1.0F));
        test_truth(ptest::internal::comp_float(1.00000001F, 1.0F));
    }
}

int main()
{
    run_tests();

    if (tests_failed > 0)
    {
        printf("FAILED !!! %zu tests out of %zu have failed...\n", tests_failed, tests_total);
    }
    else
    {
        printf("SUCCESS all %zu tests passed...\n", tests_total);
    }
    return 0;
}
