#pragma once

#include <cstdlib>
#include <cwchar>
#include <stdio.h>
#include <string.h>
#include <float.h>
#include <type_traits>


namespace ptest
{

template<typename T>
static T abs(const T &t) { return (t < 0) ? -t : t; }

bool run_all_tests(int argc, char **argv);

struct fn_base
{
    struct results
    {
        size_t count_total = 0;
        size_t count_bad = 0;
    };

    fn_base(const char *test_case_name, const char *test_name, const char *file, int line);
    virtual ~fn_base() = default;

    virtual void __test_fn(results &__res) = 0;

    // wrap members in a struct to not pollute test function scope
    struct fn_data
    {
        const char *const test_case_name;
        const char *const test_name;
        const char *const file;
        const int line;
    };
    fn_data __ptest_fn_data;
};

struct str_buffer
{
    size_t size() const { return SIZE; }

    template <typename ...Args>
    void snprintf(const char *format, Args ...args)
    {
        ::snprintf(&data[0], SIZE, format, (args)...);
    }

    static constexpr size_t SIZE = 1024;
    char data[SIZE];
};

template <typename T> struct conv
{
    static void to_str(str_buffer &buffer, const T &value)
    {
        constexpr unsigned int max_bytes_display = 400;

        size_t size = sizeof(T);

        size_t pos = 0;
        pos += snprintf(buffer.data, str_buffer::SIZE, "object at %p. size: %zu.\n        bytes: ", &value, size);

        const char *data = reinterpret_cast<const char *>(&value);
        for (size_t i = 0; i < size && i < max_bytes_display; ++i)
        {
            if (i % 40 == 0)
            {
                buffer.data[pos++] = '\n';
                for (int j = 0; j < 12; ++j)
                    buffer.data[pos++] = ' ';
            }
            else if (i % 4 == 0)
            {
                buffer.data[pos++] = ' ';
            }
            snprintf(buffer.data + pos, str_buffer::SIZE - pos, "%01x", *(data + i));
            ++pos;
        }

        if (size > max_bytes_display)
        {
            buffer.data[pos++] = '\n';
            for (int j = 0; j < 12; ++j)
                buffer.data[pos++] = ' ';
            buffer.data[pos++] = '.';
            buffer.data[pos++] = '.';
            buffer.data[pos++] = '.';
        }

        buffer.data[pos] = '\0';
    }
};

#define GEN_CONV(type, fmt) \
    template<> struct conv<type> { static void to_str(str_buffer &buffer, type value) { snprintf(buffer.data, str_buffer::SIZE, fmt, value);  } }
GEN_CONV(char, "%c");
GEN_CONV(unsigned char, "%c");
GEN_CONV(int, "%d");
GEN_CONV(unsigned int, "%u");
GEN_CONV(long, "%ld");
GEN_CONV(unsigned long, "%lu");
GEN_CONV(long long, "%lld");
GEN_CONV(unsigned long long, "%llu");
GEN_CONV(float, "%f");
GEN_CONV(double, "%f");
GEN_CONV(long double, "%Lf");
#undef GEN_CONV

template<> struct conv<bool> { static void to_str(str_buffer &buffer, const bool &value) { strncpy(buffer.data, value ? "true" : "false", buffer.size());  } };
template<> struct conv<char *> { static void to_str(str_buffer &buffer, const char *value) { strncpy(buffer.data, value ? value : "(null)", buffer.size()); } };
template<> struct conv<const char *> { static void to_str(str_buffer &buffer, const char *value) { strncpy(buffer.data, value ? value : "(null)", buffer.size());  } };
template<size_t S>
struct conv<char[S]>
{
    static void to_str(str_buffer &buffer, char *value[S])
    {
        snprintf(buffer.data, str_buffer::SIZE, "%.*s\n", S, value);
    }
};


namespace internal
{

inline bool comp_str(const char *lhs, const char *rhs)
{ return (strcmp(lhs, rhs) == 0); }
inline bool comp_str(const wchar_t *lhs, const wchar_t *rhs)
{ return (wcscmp(lhs, rhs) == 0); }

inline bool comp_float(double lhs, double rhs)
{ return (ptest::abs(lhs - rhs) < DBL_EPSILON); }


enum class OnFail { Exit, Continue };
enum class Expect { Failure, Success };

inline void report_result(const char *file, int line, ptest::fn_base::results &__res, bool failed)
{
    ++__res.count_total;
    if (failed)
    {
        ++__res.count_bad;
        printf("at %s:%d\n", file, line);
        printf("    test failed\n");
    }
}

}

template<typename T1, typename T2, typename Comparator, typename StrConv1, typename StrConv2>
bool test(const char *file, int line, ptest::fn_base::results &__res, internal::Expect should_fail,
          const char *expr_str_1, const char *expr_str_2,
          Comparator comparator,
          const T1 &expr1, const T2 &expr2,
          StrConv1 str_conv1, StrConv2 str_conv2)
{
    ++__res.count_total;
    if (comparator(expr1, expr2) != (should_fail == internal::Expect::Success))
    {
        ++__res.count_bad;
        printf("at %s:%d\n", file, line);
        str_buffer str;
        str_conv1(str, expr1);
        printf("    lhs '%s' equals '%s'\n", expr_str_1, str.data);
        str_conv2(str, expr2);
        printf("    rhs '%s' equals '%s'\n", expr_str_2, str.data);
        return false;
    }
    return true;
}

}

/// EXPLICIT SUCCESS AND FAILURE

#define SUCCEED() do { ptest::internal::report_result(__FILE__, __LINE__, __res, false); } while (0)
#define FAIL() do { ptest::internal::report_result(__FILE__, __LINE__, __res, true); return; } while (0)
#define ADD_FAILURE() do { ptest::internal::report_result(__FILE__, __LINE__, __res, true); } while (0)


/// GENERIC EQUAL

#define __TEST_EQ(expr1, expr2, exit_on_fail, should_fail)                                                             \
    do                                                                                                                 \
    {                                                                                                                  \
        bool __ok = ptest::test(                                                                                       \
            __FILE__, __LINE__, __res, should_fail, #expr1, #expr2,                                                    \
            [](const auto &e1, const auto &e2) { return (e1 == e2); }, expr1, expr2,                                   \
            [](ptest::str_buffer &buffer, const auto &arg1) {                                                          \
                return ptest::conv<std::decay_t<decltype(arg1)>>::to_str(buffer, arg1);                                \
            },                                                                                                         \
            [](ptest::str_buffer &buffer, const auto &arg2) {                                                          \
                return ptest::conv<std::decay_t<decltype(arg2)>>::to_str(buffer, arg2);                                \
            });                                                                                                        \
        if (!__ok && exit_on_fail == ptest::internal::OnFail::Exit)                                                    \
            return;                                                                                                    \
    } while (0)

#define ASSERT_EQ(expr1, expr2) __TEST_EQ(expr1, expr2, ptest::internal::OnFail::Exit, ptest::internal::Expect::Success)
#define ASSERT_NE(expr1, expr2) __TEST_EQ(expr1, expr2, ptest::internal::OnFail::Exit, ptest::internal::Expect::Failure)
#define EXPECT_EQ(expr1, expr2) __TEST_EQ(expr1, expr2, ptest::internal::OnFail::Continue, ptest::internal::Expect::Success)
#define EXPECT_NE(expr1, expr2) __TEST_EQ(expr1, expr2, ptest::internal::OnFail::Continue, ptest::internal::Expect::Failure)

/// BOOL EQUAL

#define __TEST_BOOL(expr, exit_on_fail, should_fail)                                                                   \
    do                                                                                                                 \
    {                                                                                                                  \
        bool __ok = ptest::test<bool, bool>(                                                                           \
            __FILE__, __LINE__, __res, should_fail, #expr, "true", [](bool b1, bool b2) { return (b1 == b2); }, expr,  \
            true, [](ptest::str_buffer &buffer, const auto &arg1) { return ptest::conv<bool>::to_str(buffer, arg1); }, \
            [](ptest::str_buffer &buffer, const auto &arg2) { return ptest::conv<bool>::to_str(buffer, arg2); });      \
        if (!__ok && exit_on_fail == ptest::internal::OnFail::Exit)                                                    \
            return;                                                                                                    \
    } while (0)

#define ASSERT_TRUE(expr) __TEST_BOOL(expr, ptest::internal::OnFail::Exit, ptest::internal::Expect::Success)
#define ASSERT_FALSE(expr) __TEST_BOOL(expr, ptest::internal::OnFail::Exit, ptest::internal::Expect::Failure)
#define EXPECT_TRUE(expr) __TEST_BOOL(expr, ptest::internal::OnFail::Continue, ptest::internal::Expect::Success)
#define EXPECT_FALSE(expr) __TEST_BOOL(expr, ptest::internal::OnFail::Continue, ptest::internal::Expect::Failure)

/// STR EQUAL

#define __TEST_STREQ(expr1, expr2, exit_on_fail, should_fail)                                                          \
    do                                                                                                                 \
    {                                                                                                                  \
        bool __ok = ptest::test(                                                                                       \
            __FILE__, __LINE__, __res, should_fail, #expr1, #expr2,                                                    \
            [](const auto &e1, const auto &e2) { return ptest::internal::comp_str(e1, e2); }, expr1, expr2,            \
            [](ptest::str_buffer &buffer, const auto &arg1) {                                                          \
                return ptest::conv<std::decay_t<decltype(arg1)>>::to_str(buffer, arg1);                                \
            },                                                                                                         \
            [](ptest::str_buffer &buffer, const auto &arg2) {                                                          \
                return ptest::conv<std::decay_t<decltype(arg2)>>::to_str(buffer, arg2);                                \
            });                                                                                                        \
        if (!__ok && exit_on_fail == ptest::internal::OnFail::Exit)                                                    \
            return;                                                                                                    \
    } while (0)

#define ASSERT_STREQ(expr1, expr2) __TEST_STREQ(expr1, expr2, ptest::internal::OnFail::Exit, ptest::internal::Expect::Success)
#define EXPECT_STREQ(expr1, expr2) __TEST_STREQ(expr1, expr2, ptest::internal::OnFail::Continue, ptest::internal::Expect::Success)
#define ASSERT_STRNE(expr1, expr2) __TEST_STREQ(expr1, expr2, ptest::internal::OnFail::Exit, ptest::internal::Expect::Failure)
#define EXPECT_STRNE(expr1, expr2) __TEST_STREQ(expr1, expr2, ptest::internal::OnFail::Continue, ptest::internal::Expect::Failure)

/// FLOAT EQUAL
/// @todo float eq is probably not right
#define __TEST_FLOAT_EQ(expr1, expr2, exit_on_fail, should_fail)                                                       \
    do                                                                                                                 \
    {                                                                                                                  \
        bool __ok = ptest::test(                                                                                       \
            __FILE__, __LINE__, __res, should_fail, #expr1, #expr2,                                                    \
            [](const auto &e1, const auto &e2) { return ptest::internal::comp_float(e1, e2); }, expr1, expr2,          \
            [](ptest::str_buffer &buffer, const auto &arg1) {                                                          \
                return ptest::conv<std::decay_t<decltype(arg1)>>::to_str(buffer, arg1);                                \
            },                                                                                                         \
            [](ptest::str_buffer &buffer, const auto &arg2) {                                                          \
                return ptest::conv<std::decay_t<decltype(arg2)>>::to_str(buffer, arg2);                                \
            });                                                                                                        \
        if (!__ok && exit_on_fail == ptest::internal::OnFail::Exit)                                                    \
            return;                                                                                                    \
    } while (0)

#define ASSERT_FLOAT_EQ(expr1, expr2) __TEST_FLOAT_EQ(expr1, expr2, ptest::OnFail::Exit, ptest::internal::Expect::Success)
#define EXPECT_FLOAT_EQ(expr1, expr2) __TEST_FLOAT_EQ(expr1, expr2, ptest::internal::OnFail::Continue, ptest::internal::Expect::Success)
#define ASSERT_FLOAT_NE(expr1, expr2) __TEST_FLOAT_EQ(expr1, expr2, ptest::OnFail::Exit, ptest::internal::Expect::Failure)
#define EXPECT_FLOAT_NE(expr1, expr2) __TEST_FLOAT_EQ(expr1, expr2, ptest::internal::OnFail::Continue, ptest::internal::Expect::Failure)

#define IMPL_PTEST_CLASS_NAME(test_case_name, test_name) ptest_class##test_case_name##test_name

#define TEST(test_case_name, test_name)                                                                                \
    static_assert(sizeof((#test_case_name)) > 1, "test_case_name must not be empty");                                  \
    static_assert(sizeof((#test_name)) > 1, "test_name must not be empty");                                            \
    namespace ptest_fn_namespace                                                                                       \
    {                                                                                                                  \
    struct IMPL_PTEST_CLASS_NAME(test_case_name, test_name) : public ptest::fn_base                                    \
    {                                                                                                                  \
        IMPL_PTEST_CLASS_NAME(test_case_name, test_name)                                                               \
        () : ptest::fn_base(#test_case_name, #test_name, __FILE__, __LINE__)                                           \
        {                                                                                                              \
        }                                                                                                              \
        void __test_fn(results &__res) override;                                                                       \
    };                                                                                                                 \
    IMPL_PTEST_CLASS_NAME(test_case_name, test_name) ptest_class_instance_##test_case_name##test_name;                 \
    }                                                                                                                  \
    void ptest_fn_namespace::IMPL_PTEST_CLASS_NAME(test_case_name,                                                     \
                                                   test_name)::__test_fn([[maybe_unused]] results &__res)
