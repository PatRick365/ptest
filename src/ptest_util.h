#include <string.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#define PTEST_ASSERT(x)                                                                                                \
    do                                                                                                                 \
    {                                                                                                                  \
        if (!static_cast<bool>(x))                                                                                     \
        {                                                                                                              \
            fprintf(stderr, "FATAL ERROR AT %s:%d (%s)\n", __FILE__, __LINE__, #x);                                    \
            abort();                                                                                                   \
        }                                                                                                              \
    } while (0)

class string
{
public:
    string() = default;
    string(const char *str)
        : m_data(strdup(str))
    {}
    string(const string &other)
        : m_data(strdup(other.m_data))
    {}
    string(string &&other)
        : m_data(other.m_data)
    {
        other.m_data = nullptr;
    }
    template<typename ...Args>
    string(const char *fmt, Args &&...args)
    {
        int len = snprintf(nullptr, 0, fmt, args...);
        m_data = static_cast<char *>(malloc(len + 1));
        snprintf(m_data, len + 1, fmt, args...);
    }

    ~string() { free(m_data); }

    // other option
    // template<typename ...Args>
    // static string format(const char *fmt, Args &&...args)
    // {
    //     string str;
    //     int len = snprintf(nullptr, 0, fmt, args...);
    //     str.m_data = static_cast<char *>(malloc(len + 1));
    //     snprintf(str.m_data, len + 1, fmt, args...);
    //     return str;
    // }

    string &operator=(const string &other)
    {
        if (this != &other)
        {
            free(m_data);
            m_data = strdup(other.m_data);
        }
        return *this;
    }
    string &operator=(string &&other)
    {
        if (this != &other)
        {
            free(m_data);
            m_data = other.m_data;
            other.m_data = nullptr;
        }
        return *this;
    }

    template<typename ...Args>
    string &format(const char *fmt, Args &&...args)
    {
        free(m_data);
        int len = snprintf(nullptr, 0, fmt, args...);
        m_data = static_cast<char *>(realloc(m_data, len + 1));
        snprintf(m_data, len + 1, fmt, args...);
        return *this;
    }

    const char *c_str() const { return m_data; }

private:
    char *m_data = nullptr;
};

template<typename T>
class list
{
public:
    struct node
    {
        T value;
        node *next = nullptr;
    };

    list() = default;

    ~list() { clear(); }

    list(const list &other)
    {
        for (iterator it(other); it.valid(); ++it)
        {
            push_back(*it);
        }
    }

    list &operator=(const list &other)
    {
        clear();

        for (iterator it(other); it.valid(); ++it)
        {
            push_back(*it);
        }
        return *this;
    }

    list(list &&other)
    {
        m_head = other.m_head;
        other.m_head = nullptr;
    }

    list &operator=(list &&other)
    {
        m_head = other.m_head;
        other.m_head = nullptr;
        return *this;
    }

    void push_back(const T &value)
    {
        node *new_node = new node{value, nullptr};
        if (!m_head)
        {
            m_head = new_node;
        }
        else
        {
            node *node = m_head;
            while (node->next)
            {
                node = node->next;
            }
            node->next = new_node;
        }
    }

    void clear()
    {
        node *node_prev = nullptr;

        node *node = m_head;

        while (node)
        {
            node_prev = node;
            node = node->next;
            delete node_prev;
        }
        m_head = nullptr;
    }

    size_t size() const
    {
        size_t sz = 0;

        for (const_iterator it(*this); it.valid(); ++it)
        {
            ++sz;
        }

        return sz;
    }

    bool empty() const { return (m_head == nullptr); }

    class iterator
    {
    public:
        iterator() = default;
        explicit iterator(const list &list) : m_node(list.m_head) {}
        iterator &operator++()
        {
            PTEST_ASSERT(valid());
            m_node = m_node->next;
            return *this;
        }
        T *operator->()
        {
            PTEST_ASSERT(valid());
            return &m_node->value;
        }
        T &operator*()
        {
            PTEST_ASSERT(valid());
            return m_node->value;
        }
        bool valid() const { return (m_node != nullptr); }

    private:
        node *m_node = nullptr;
    };
    class const_iterator
    {
    public:
        const_iterator() = default;
        explicit const_iterator(const list &list) : m_node(list.m_head) {}
        const_iterator &operator++()
        {
            PTEST_ASSERT(valid());
            m_node = m_node->next;
            return *this;
        }
        const T *operator->()
        {
            PTEST_ASSERT(valid());
            return &m_node->value;
        }
        const T &operator*()
        {
            PTEST_ASSERT(valid());
            return m_node->value;
        }
        bool valid() const { return (m_node != nullptr); }

    private:
        const node *m_node = nullptr;
    };

private:
    node *m_head = nullptr;
};

class time_interval
{
public:
    time_interval() = default;

    explicit time_interval(struct timeval time) : m_time(time) {}

    time_interval &operator+=(time_interval &rhs)
    {
        struct timeval result;
        timeradd(&m_time, &rhs.m_time, &result);
        m_time = result;
        return *this;
    }

    time_interval operator+(time_interval rhs)
    {
        struct timeval result;
        timeradd(&m_time, &rhs.m_time, &result);
        return time_interval(result);
    }

    string to_str() const
    {
        long ms = (m_time.tv_sec * 1000) + m_time.tv_usec / 1000;
        long us = m_time.tv_usec % 1000;
        return string("%ld.%03ld ms", ms, us);
    }

private:
    struct timeval m_time{0, 0};
};

class timer
{
public:
    timer() { reset(); }
    void reset() { gettimeofday(&m_start, nullptr); }

    time_interval elapsed() const
    {
        struct timeval end, elapsed;
        gettimeofday(&end, nullptr);
        timersub(&end, &m_start, &elapsed);
        return time_interval(elapsed);
    }

private:
    struct timeval m_start{0, 0};
};
