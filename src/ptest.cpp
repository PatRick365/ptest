#include "ptest/ptest.h"
#include "ptest_util.h"

#include <assert.h>
#include <sys/time.h>


#define RED(str) "\033[1;31m" str "\033[0m"
#define YELLOW(str) "\033[1;33m" str "\033[0m"
#define GREEN(str) "\033[0;32m" str "\033[0m"

namespace ptest
{

struct test_case
{
    const char *test_case_name;
    list<fn_base *> lst_tests;
};

class test_case_collection
{
public:
    void add_test(fn_base *test)
    {
        for (list<test_case>::iterator it(m_lst_test_cases); it.valid(); ++it)
        {
            if (strcmp(test->__ptest_fn_data.test_case_name, it->test_case_name) == 0)
            {
                it->lst_tests.push_back(test);
                return;
            }
        }
        test_case test_case;
        test_case.test_case_name = test->__ptest_fn_data.test_case_name;
        test_case.lst_tests.push_back(test);
        m_lst_test_cases.push_back(test_case);
    }

    size_t size() const
    {
        size_t sz = 0;
        for (list<test_case>::const_iterator it(m_lst_test_cases); it.valid(); ++it)
        {
            sz += it->lst_tests.size();
        }
        return sz;
    }

    const list<test_case> &lst_test_cases() const
    {
        return m_lst_test_cases;
    }

private:
    list<test_case> m_lst_test_cases;
};

static test_case_collection &get_test_list()
{
    static test_case_collection lst;
    return lst;
}

ptest::fn_base::fn_base(const char *test_case_name, const char *test_name, const char *file, int line)
    : __ptest_fn_data { test_case_name, test_name, file, line }
{
    get_test_list().add_test(this);
}

class test_filter
{
public:
    explicit test_filter(const char *str)
        : m_filter_test_case(strdup(str)),
          m_filter_test(nullptr)
    {
        m_filter_test = strstr(m_filter_test_case, ".");
        if (m_filter_test)
        {
            m_filter_test[0] = '\0';
            ++m_filter_test;
        }
    }
    test_filter(const test_filter &) = delete;
    test_filter &operator=(const test_filter &) = delete;

    ~test_filter()
    {
        free(m_filter_test_case);
    }

    bool match_test_case(const char *test_case_name)
    {
        assert(strlen(test_case_name) > 0);
        return (!m_filter_test_case || (strcmp(m_filter_test_case, test_case_name) == 0));
    }
    bool match_test(const char *test_name)
    {
        assert(strlen(test_name) > 0);
        return (!m_filter_test || (strcmp(m_filter_test, test_name) == 0));
    }

private:
    char *m_filter_test_case;
    char *m_filter_test;
};

bool run_test_cases(const test_case_collection &lst_tests)
{
    list<ptest::fn_base *> lst_failed_tests;
    time_interval elapsed_total;

    printf(GREEN("[========]")" running %zu test%s from %zu test case%s\n\n", lst_tests.size(), (lst_tests.size() > 1 ? "s" : ""), lst_tests.lst_test_cases().size(), (lst_tests.lst_test_cases().size() > 1 ? "s" : ""));

    for (list<test_case>::const_iterator it_test_case(lst_tests.lst_test_cases()); it_test_case.valid(); ++it_test_case)
    {
        printf(GREEN("[--------]")" %zu test%s from %s\n", it_test_case->lst_tests.size(), (it_test_case->lst_tests.size() > 1 ? "s" : ""), it_test_case->test_case_name);
        time_interval elapsed_test_case;
        for (list<ptest::fn_base *>::const_iterator it_test(it_test_case->lst_tests); it_test.valid(); ++it_test)
        {
            ptest::fn_base::results res;
            printf(GREEN("[ RUN    ]")" %s.%s\n", (*it_test)->__ptest_fn_data.test_case_name, (*it_test)->__ptest_fn_data.test_name);
            timer timer;
            (*it_test)->__test_fn(res);
            time_interval elapsed_test = timer.elapsed();
            elapsed_test_case += elapsed_test;

            if (res.count_bad > 0)
            {
                lst_failed_tests.push_back(*it_test);
                printf(RED("[ FAILED ]")" %s.%s (%s\n", (*it_test)->__ptest_fn_data.test_case_name, (*it_test)->__ptest_fn_data.test_name, elapsed_test.to_str().c_str());
            }
            else
            {
                printf(GREEN("[     OK ]")" %s.%s (%s)\n", (*it_test)->__ptest_fn_data.test_case_name, (*it_test)->__ptest_fn_data.test_name, elapsed_test.to_str().c_str());
            }
        }
        elapsed_total += elapsed_test_case;
        printf(GREEN("[--------]")" %zu test%s from %s (%s total)\n\n", it_test_case->lst_tests.size(), (it_test_case->lst_tests.size() > 1 ? "s" : ""), it_test_case->test_case_name, elapsed_test_case.to_str().c_str());
    }

    printf(GREEN("[========]")" %zu test%s ran\n", lst_tests.size(), (lst_tests.size() > 1 ? "s" : ""));

    if (lst_failed_tests.empty())
    {
        printf(GREEN("[ PASSED ]")" all tests passed! (%s total)\n", elapsed_total.to_str().c_str());
    }
    else
    {
        printf(RED("[ FAILED ]")" %zu test%s failed! list is below. (%s total)\n", lst_failed_tests.size(), (lst_failed_tests.size() > 1 ? "s" : ""), elapsed_total.to_str().c_str());
    }

    for (list<ptest::fn_base *>::const_iterator it(lst_failed_tests); it.valid(); ++it)
    {
        printf(RED("[ FAILED ]")" %s.%s\n", (*it)->__ptest_fn_data.test_case_name, (*it)->__ptest_fn_data.test_name);
    }

    return lst_failed_tests.empty();
}


bool run_all_tests(int argc, char **argv)
{
    const test_case_collection &lst_all_tests = get_test_list();

    if (argc > 1)
    {
        size_t length = strlen(argv[0]);
        if (length == 0)
        {
            fprintf(stderr, "filter error: empty string.");
            return false;
        }

        int count_dots = 0;
        for (size_t i = 0; i < length; ++i)
        {
            if (argv[0][i] == '.')
            {
                if (i == 0)
                {
                    fprintf(stderr, "filter error: empty test case name.");
                    return false;
                }

                ++count_dots;
            }
        }

        if (count_dots > 1)
        {
            fprintf(stderr, "filter error: to many dots.");
            return false;
        }

        test_filter filter(argv[1]);

        test_case_collection lst_tests;

        for (list<test_case>::const_iterator it_test_case(lst_all_tests.lst_test_cases()); it_test_case.valid(); ++it_test_case)
        {
            if (!filter.match_test_case(it_test_case->test_case_name))
                continue;

            for (list<ptest::fn_base *>::const_iterator it_test(it_test_case->lst_tests); it_test.valid(); ++it_test)
            {
                if (!filter.match_test((*it_test)->__ptest_fn_data.test_name))
                    continue;

                lst_tests.add_test(*it_test);
            }
        }

        if (lst_tests.lst_test_cases().empty())
        {
            fprintf(stderr, "filter error: no tests found with filter.");
            return false;
        }

        return run_test_cases(lst_tests);
    }
    else
    {
        return run_test_cases(lst_all_tests);
    }
}

}
